<?php

/**
 * @file quicktables-edit-content-form.tpl.php
 */

  // Add table javascript.
  drupal_add_js('misc/tableheader.js');
  drupal_add_tabledrag('quicktable', 'order', 'self', 'row-weight');
?>
<table id="quicktable" class="sticky-enabled">
  <thead>
    <tr>
      <?php
        print $form_header;
      ?>
      <th></th>
    </tr>
  </thead>
  <tbody class="dragging">
    <?php
      print $form_rows;
    ?>
  </tbody>
</table>

<?php print $form_submit; ?>

<table>
  <tbody>
  </tbody>
</table>

